package me.aspardo.calci;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import me.aspardo.R;

public class MainActivity extends AppCompatActivity {


    private TextView mShowCount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("MainActivity", "Hello World");

        mShowCount = (TextView) findViewById(R.id.show_count);
        mShowCount.setMovementMethod(new ScrollingMovementMethod());
        mShowCount.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if(mShowCount.getText().toString().length()>7) {
                    mShowCount.setTextSize(50);
                }
                else {
                    mShowCount.setTextSize(100);
                }

            }

        });
    }



    @SuppressLint("SetTextI18n")
    public void countUp(View view) {


        assert mShowCount != null;
        try {
            String result = Double.toString(eval(mShowCount.getText().toString()));

            mShowCount.setText(result);
        } catch (Exception e) {
            e.printStackTrace();
            mShowCount.setText("Math error");
        }


    }

    public void append(View view) {
        Button b = (Button)view;
        if(mShowCount.getText().toString().equals("0")) {
            mShowCount.setText(b.getText().toString());
        } else {
            mShowCount.append(b.getText().toString());


        }

    }

    public static double eval(final String str) {
        Expression e = new ExpressionBuilder(str)
                .build();

        return e.evaluate();
    }

    @SuppressLint("SetTextI18n")
    public void cleartxt(View view) {
        mShowCount.setText(Integer.toString(0));
    }

    public void appendsq(View view) {

        if(mShowCount.getText().toString().equals("0")) {
            mShowCount.setText("sqrt");
        } else {
            mShowCount.append("sqrt");


        }
    }

    public void clearl(View view) {

            String str = mShowCount.getText().toString();
            if(str.length()!=0) {
                mShowCount.setText(str.substring(0, str.length() - 1));
            }
            else mShowCount.setText("0");





    }

}
